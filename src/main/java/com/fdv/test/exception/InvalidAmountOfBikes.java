package com.fdv.test.exception;

public class InvalidAmountOfBikes extends RuntimeException {
    public InvalidAmountOfBikes() {
        super("Amount of bikes cannot be 0 or negative number");
    }
}
