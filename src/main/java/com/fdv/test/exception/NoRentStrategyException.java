package com.fdv.test.exception;

public class NoRentStrategyException extends RuntimeException {
    public NoRentStrategyException() {
        super("No valid time format");
    }
}
