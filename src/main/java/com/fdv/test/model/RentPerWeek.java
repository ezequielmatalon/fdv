package com.fdv.test.model;

import com.fdv.test.strategy.DiscountStrategy;
import com.fdv.test.strategy.RentStrategy;

public class RentPerWeek implements RentStrategy {

    public Integer getStrategyPrice() {
        return 60;
    }
}
