package com.fdv.test.model;

public enum RentTime {
    HOUR, DAY, WEEK, MONTH
}
