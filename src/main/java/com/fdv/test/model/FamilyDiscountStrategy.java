package com.fdv.test.model;

import com.fdv.test.strategy.DiscountStrategy;

public class FamilyDiscountStrategy implements DiscountStrategy {
    public Double applyDiscount(Integer price) {
        return price - (price * 0.3);
    }
}
