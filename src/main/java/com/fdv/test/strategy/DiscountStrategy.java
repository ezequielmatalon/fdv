package com.fdv.test.strategy;

public interface DiscountStrategy {
    Double applyDiscount(Integer price);
}
