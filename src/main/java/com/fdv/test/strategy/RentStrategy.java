package com.fdv.test.strategy;

public interface RentStrategy {
    default Double price(Integer amountOfBikes) {
        Integer price = getStrategyPrice() * amountOfBikes;
        DiscountStrategy discountStrategy = DiscountStrategyFactory.getStrategy(amountOfBikes);
        if (discountStrategy == null) {
            return price.doubleValue();
        }

        return discountStrategy.applyDiscount(price);
    }

    Integer getStrategyPrice();
}
