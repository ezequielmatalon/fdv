package com.fdv.test.strategy;

import com.fdv.test.model.FamilyDiscountStrategy;

public class DiscountStrategyFactory {

    public static DiscountStrategy getStrategy(Integer amountOfBikes) {
        if (amountOfBikes >= 3 && amountOfBikes <= 5) {
            return new FamilyDiscountStrategy();
        }

        return null;
    }
}
