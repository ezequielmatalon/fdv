package com.fdv.test.strategy;

import com.fdv.test.exception.NoRentStrategyException;
import com.fdv.test.model.*;

public class RentStrategyFactory {
    public static RentStrategy getStrategy(RentTime time) {
        if (time == null) {
            throw new NoRentStrategyException();
        }
        
        switch (time) {
            case HOUR:
                return new RentPerHour();
            case DAY:
                return new RentPerDay();
            case WEEK:
                return new RentPerWeek();
            default:
                throw new NoRentStrategyException();
        }

    }
}
