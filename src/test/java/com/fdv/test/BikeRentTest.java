package com.fdv.test;

import com.fdv.test.exception.InvalidAmountOfBikes;
import com.fdv.test.exception.NoRentStrategyException;
import com.fdv.test.model.BikeRent;
import com.fdv.test.model.RentTime;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class BikeRentTest {

    BikeRent bikeRent;

    @BeforeClass
    public void init() {
        bikeRent = new BikeRent();
    }

    @Test
    @Parameters({"amount", "time", "finalPrice"})
    public void rentABike(Integer amount, RentTime time, Double finalPrice) {
        Assert.assertEquals(bikeRent.rent(time, amount), finalPrice);
    }

    @Test(expectedExceptions = {NoRentStrategyException.class, InvalidAmountOfBikes.class})
    @Parameters({"amount", "time"})
    public void invalidArguments(Integer amount, @Optional RentTime time) {
        bikeRent.rent(time, amount);
    }
}
